/*
 * system.h
 *
 *  Rev:
 *  Author:
 *  Date:
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "S32K144.h"

/* Include here BSP settings */
#define S32K144_FTM_CHANNELS 32


/* Include here BSP settings */
/* Clock settings */
#define OSC_VALUE           (8000000u)
#define SIRC_VALUE          (8000000u)
#define FIRC_VALUE          (48000000u)

#define OSCDIV1_FREQ 8000000
#define FIRCDIV1_FREQ 8000000


/* FTM pointer */
typedef void (*FTM_callback)(uint32_t);

typedef enum{
	INT_EN,
	INT_DIS
}interrupt_t;

#endif /* SYSTEM_H_ */

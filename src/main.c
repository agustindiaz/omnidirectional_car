/*
 * main implementation: use this 'C' sample to create your own application
 *
 */


#include "OmniRobot.h"
#include "Encoder.h"
#include "lpuart.h"
#include "scg.h" /* include peripheral declarations S32K144 */
#include "FTM.h"


/* configuration structures */
extern SCG_config_t configStruct;

uint16_t encoder_values_test[4] = {0};
uint32_t core_clock,bus_clock,flash_clock;

int main(void)
{
	uint8_t data_received;
	SCG_init(&configStruct);
    SCG_set_clock_freq(eSysOsc_div1,OSCDIV1_FREQ);
    SCG_set_clock_freq(eSysOsc_div2,OSCDIV1_FREQ);

    core_clock = SCG_get_clock_freq(eCoreClock);
    bus_clock = SCG_get_clock_freq(eBusClock);
    flash_clock = SCG_get_clock_freq(eFlashClock);


    Omnirobot_init();
    Encoder_init();
    LPUART1_init();


	for(;;) {
		Omnirobot_move(Backward,50,30,50,75);
		//data_received = LPUART1_receive_char();
//		data_received = 'w';
//		if(data_received == 'w'){
//		    Omnirobot_move(Foward,20,20,20,20);
//		}
//		else if(data_received == 's'){
//		    Omnirobot_move(Backward,20,20,20,20);
//		}
//		else if(data_received == 'd'){
//		    Omnirobot_move(Right,40,40,40,40);
//		}
//
//		else if(data_received == 'a'){
//		    Omnirobot_move(Left,40,40,40,40);
//		}
//		else if(data_received == 'e'){
//		    Omnirobot_move(Diagonal_up_right,20,20,20,20);
//		}
//
//		else if(data_received == 'q'){
//		    Omnirobot_move(Diagonal_up_left,20,20,20,20);
//		}
//
//		else if(data_received == 'z'){
//		    Omnirobot_move(Diagonal_down_left,20,20,20,20);
//		}
//
//		else if(data_received == 'c'){
//		    Omnirobot_move(Diagonal_down_right,20,20,20,20);
//		}
//
		Encoder_get_val(&encoder_values_test[0]);


	}

}

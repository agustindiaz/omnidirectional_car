/*
 * template_config.h
 *
 *  Rev:
 *  Author:
 *  Date:
 */

#ifndef FTM_CONFIG_H_
#define FTM_CONFIG_H_

#include "system.h"

typedef enum{
	TCLK = 0,
	SOSCDIV1_CLK = 1,
	SIRCDIV1_CLK = 2,
	FIRCDIV1_CLK = 3,
	SPLLDIV1_CLK = 6
}FTM_clock_source_t;

typedef enum{
	input_capture_mode,
	output_compare_mode,
	PWM_mode
}FTM_channel_mode_t;

typedef enum{
	capture_rise_edge,
	capture_fall_edge,
	capture_any_edge,
	no_capture
}FTM_input_capture_mode_t;

typedef enum{
	toggle_output
}FTM_output_compare_mode_t;

typedef enum{
	center_aligned,
	edge_aligned,
	combine
}FTM_PWM_mode_t;

typedef struct{
	FTM_PWM_mode_t PWM_mode;
	uint32_t PWM_frequency;
	uint8_t PWM_duty_cycle;
}FTM_PWM_channel_config_t;

typedef struct{
	FTM_input_capture_mode_t InputCapture_mode;
	interrupt_t interrupt_setting;
	FTM_callback CallbackPointer;
}FTM_input_capture_channel_config_t;

typedef struct{
	FTM_output_compare_mode_t OutputCompare_mode;
	uint32_t period_us;
	interrupt_t interrupt_setting;
	FTM_callback CallbackPointer;
}FTM_output_compare_channel_config_t;


typedef union{
	FTM_PWM_channel_config_t PWM_config;
	FTM_input_capture_channel_config_t InputCapture_cofig;
	FTM_output_compare_channel_config_t OutputCompare_config;
}FTM_mode_config_t;

typedef struct{
	uint8_t channel;
	FTM_channel_mode_t FTMmode;
	FTM_mode_config_t configMode;
}FTM_channel_config_t;

/* General comment about structure */
typedef struct {
	FTM_Type * FTM_instance;
	FTM_clock_source_t FTM_clock_souce;
	FTM_channel_config_t* FTM_configured_channels;
	uint8_t number_of_channels;
}FTM_config_t;

#endif /* FTM_CONFIG_H */

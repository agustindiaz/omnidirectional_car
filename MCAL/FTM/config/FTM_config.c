/*
 * template_config.c
 *
 *  Created on: Feb 10, 2017
 *      Author: B50982
 */


#include "FTM_config.h"
#include "motor_config.h"
#include "encoder_config.h"


extern  FTM_callback encoder1_value(uint32_t compare_value);
extern  FTM_callback encoder2_value(uint32_t compare_value);
extern  FTM_callback encoder3_value(uint32_t compare_value);
extern  FTM_callback encoder4_value(uint32_t compare_value);

FTM_channel_config_t FTM_PWM_channels_configuration[4] = {
		/* channel 0 intialization */
		{
				.channel = MOTOR1_PWM_CHANNEL,
				.FTMmode = PWM_mode,
				.configMode.PWM_config = {
						.PWM_mode = edge_aligned,
						.PWM_frequency = 1000,
						.PWM_duty_cycle = 0
				}
		},

		/* channel 1 initialization */
		{
				.channel = MOTOR2_PWM_CHANNEL,
				.FTMmode = PWM_mode,
				.configMode.PWM_config = {
						.PWM_mode = edge_aligned,
						.PWM_frequency = 1000,
						.PWM_duty_cycle = 0
				}
		},
		/* channel 2 initialization */
		{
				.channel = MOTOR3_PWM_CHANNEL,
				.FTMmode = PWM_mode,
				.configMode.PWM_config = {
						.PWM_mode = edge_aligned,
						.PWM_frequency = 1000,
						.PWM_duty_cycle = 0
				}
		},

		/* channel 3 initialization */
		{
				.channel = MOTOR4_PWM_CHANNEL,
				.FTMmode = PWM_mode,
				.configMode.PWM_config = {
						.PWM_mode = edge_aligned,
						.PWM_frequency = 1000,
						.PWM_duty_cycle = 0
				}
		}
};

FTM_channel_config_t FTM_InputCapture_channels_configuration[4] = {
		/* channel 0 initialization */
		{
				.channel = MOTOR1_ENCODER_CHANNEL,
				.FTMmode = input_capture_mode,
				.configMode.InputCapture_cofig = {
						.InputCapture_mode = capture_any_edge,
						.interrupt_setting = INT_EN,
						.CallbackPointer = encoder1_value
				}
		},

		/* channel 1 initialization */
		{
				.channel = MOTOR2_ENCODER_CHANNEL,
				.FTMmode = input_capture_mode,
				.configMode.InputCapture_cofig = {
						.InputCapture_mode = capture_any_edge,
						.interrupt_setting = INT_EN,
						.CallbackPointer = encoder2_value
				}
		},

		/* channel 2 initialization */
		{
				.channel = MOTOR3_ENCODER_CHANNEL,
				.FTMmode = input_capture_mode,
				.configMode.InputCapture_cofig = {
						.InputCapture_mode = capture_any_edge,
						.interrupt_setting = INT_EN,
						.CallbackPointer = encoder3_value
				}
		},

		/* channel 3 initialization */
		{
				.channel = MOTOR4_ENCODER_CHANNEL,
				.FTMmode = input_capture_mode,
				.configMode.InputCapture_cofig = {
						.InputCapture_mode = capture_any_edge,
						.interrupt_setting = INT_EN,
						.CallbackPointer = encoder4_value
				}
		}
};





FTM_config_t FTM_Motor_config = {
		.FTM_instance = OMNIROBOT_MOTORS,
		.FTM_clock_souce = SOSCDIV1_CLK,
		.FTM_configured_channels = &FTM_PWM_channels_configuration[0],
		.number_of_channels = 4
};

FTM_config_t FTM_Encoder_config = {
		.FTM_instance = OMNIROBOT_ENCODERS,
		.FTM_clock_souce = SOSCDIV1_CLK,
		.FTM_configured_channels = &FTM_InputCapture_channels_configuration[0],
		.number_of_channels = 4
};



/* comment */

/*
 * template.c
 *
 *  Created on: Feb 10, 2017
 *      Author: B50982
 */

#include "FTM.h"
#include "nvic.h"
/* Includes */
/* Just include header that will be used here */


/* MACROS */


/* Variables */

static uint8_t selected_prescaler;
static uint32_t FTM_frequency;

/* Helper Functions */
/**************************************************************************//*!
 *
 * @name    DRIVER_helper_function
 *
 * @brief   Helper funciton for DRIVER module
 *
 * @param   none.
 *
 * @return  none.
 *****************************************************************************/
static eErrorCodes FTM_pwm_channel_init(FTM_Type* FTM_base, uint8_t channel, FTM_mode_config_t channel_configuration);
static eErrorCodes FTM_input_capture_channel_init(FTM_Type* FTM_base,uint8_t channel, FTM_mode_config_t channel_configuration);
static eErrorCodes FTM_output_compare_channel_init(uint8_t channel, FTM_mode_config_t channel_configuration);
static FTM_prescaler_selection_t FTM_calculate_div(FTM_clock_source_t clock_source);
static uint32_t FTM_claculate_error(uint32_t current_value, uint32_t target_value);

static void FTM0_handler(uint8_t channel);
static void FTM1_handler(uint8_t channel);
static void FTM2_handler(uint8_t channel);
static void FTM3_handler(uint8_t channel);

/* channel independant callbacks for FTM0*/
static FTM_callback FTM0_ch_callback[8];
static FTM_callback FTM1_ch_callback[8];
static FTM_callback FTM2_ch_callback[8];
static FTM_callback FTM3_ch_callback[8];


eErrorCodes FTM_init(FTM_config_t *config_ptr){
	uint8_t FTM_channel_index;
	FTM_prescaler_selection_t FTM_preescaler_selection;
	if(config_ptr->FTM_instance == FTM0){
		PCC->PCCn[PCC_FTM0_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
		PCC->PCCn[PCC_FTM0_INDEX] |= PCC_PCCn_PCS(config_ptr->FTM_clock_souce)/* Clock Src= 1 (SOSCDIV1_CLK) */
																		| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */
		for(FTM_channel_index = 0; FTM_channel_index<8; FTM_channel_index++){
			FTM0_ch_callback[FTM_channel_index] = NULL; /* clear callback values */
		}
	}
	else if(config_ptr->FTM_instance == FTM1){
		PCC->PCCn[PCC_FTM1_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
		PCC->PCCn[PCC_FTM1_INDEX] |= PCC_PCCn_PCS(config_ptr->FTM_clock_souce)/* Clock Src= 1 (SOSCDIV1_CLK) */
																		| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */
	}
	else if(config_ptr->FTM_instance == FTM2){
		PCC->PCCn[PCC_FTM2_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
		PCC->PCCn[PCC_FTM2_INDEX] |= PCC_PCCn_PCS(config_ptr->FTM_clock_souce)/* Clock Src= 1 (SOSCDIV1_CLK) */
																		| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */
	}
	else if(config_ptr->FTM_instance == FTM3){
		PCC->PCCn[PCC_FTM3_INDEX] &= ~PCC_PCCn_CGC_MASK; /* Ensure clk disabled for config */
		PCC->PCCn[PCC_FTM3_INDEX] |= PCC_PCCn_PCS(config_ptr->FTM_clock_souce)/* Clock Src= 1 (SOSCDIV1_CLK) */
																		| PCC_PCCn_CGC_MASK; /* Enable clock for FTM regs */
	}
	FTM_preescaler_selection = FTM_calculate_div(config_ptr->FTM_clock_souce);
	config_ptr->FTM_instance->SC |= FTM_SC_PS(FTM_preescaler_selection); /* FTM timer divided by 16 */

	for(FTM_channel_index = 0; FTM_channel_index< config_ptr->number_of_channels;  FTM_channel_index++){
		if(config_ptr->FTM_configured_channels[FTM_channel_index].FTMmode == PWM_mode){
			FTM_pwm_channel_init(config_ptr->FTM_instance,config_ptr->FTM_configured_channels[FTM_channel_index].channel,
					config_ptr->FTM_configured_channels[FTM_channel_index].configMode);
		}
		else if(config_ptr->FTM_configured_channels[FTM_channel_index].FTMmode == input_capture_mode){
			FTM_input_capture_channel_init(config_ptr->FTM_instance,config_ptr->FTM_configured_channels[FTM_channel_index].channel,
					config_ptr->FTM_configured_channels[FTM_channel_index].configMode);
		}
		else if(config_ptr->FTM_configured_channels[FTM_channel_index].FTMmode == output_compare_mode){
			FTM_output_compare_channel_init(config_ptr->FTM_configured_channels[FTM_channel_index].channel,
					config_ptr->FTM_configured_channels[FTM_channel_index].configMode);
		}
		else{
			return eInvalidParameter;
		}
	}



	/* FTM1 Period = MOD-CNTIN+0x0001 ~= 62500 ctr clks  */
	/* 8MHz /128 = 62.5kHz ->  ticks -> 1Hz */
	config_ptr->FTM_instance->SC |= FTM_SC_CLKS(3); /* start FTM timer with external clock */
	return eNoError;
}

eErrorCodes PWM_set_duty(FTM_Type* FTM_base, uint8_t channel, uint8_t duty_cycle){
	FTM_base -> CONTROLS[channel].CnV = (((FTM_base->MOD) + 1)*duty_cycle)/100; /* calculate desired duty cycle value */
	return eNoError;
}

uint8_t InputCapture_event(FTM_Type* FTM_base, uint8_t channel){
	if(((FTM_base -> CONTROLS[channel].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM_base -> CONTROLS[channel].CnSC &= ~FTM_CnSC_CHF_MASK;
		return true;
	}
	else{
		return false;
	}
}

FTM_prescaler_selection_t FTM_calculate_div(FTM_clock_source_t clock_source){
	uint32_t aux_divider = 1;
	uint32_t current_frequency_error = 0;
	uint32_t last_frequency_error = 0xFFFFFFFF;
	uint8_t FTM_index = 0;

	if(clock_source == TCLK){

	}
	else if(clock_source == SOSCDIV1_CLK){
		FTM_frequency = OSCDIV1_FREQ;
	}
	else if(clock_source == SIRCDIV1_CLK){

	}
	else if(clock_source == FIRCDIV1_CLK){
		FTM_frequency = FIRCDIV1_FREQ;
	}
	else if(clock_source == SPLLDIV1_CLK){

	}


	for(FTM_index = 0; FTM_index < 8; FTM_index++){
		current_frequency_error = FTM_claculate_error(FTM_frequency/aux_divider, FTM_base_frequency);
		if(current_frequency_error >= last_frequency_error){
			aux_divider = aux_divider/2;
			selected_prescaler = aux_divider;
			if(aux_divider == 1){
				return PS_1;
			}
			else if(aux_divider == 2){
				return PS_2;
			}
			else if(aux_divider == 4){
				return PS_4;
			}
			else if(aux_divider == 8){
				return PS_8;
			}
			else if(aux_divider == 16){
				return PS_16;
			}
			else if(aux_divider == 32){
				return PS_32;
			}
			else if(aux_divider == 64){
				return PS_64;
			}
			else if(aux_divider == 128){
				return PS_128;
			}
		}
		else{
			last_frequency_error = current_frequency_error;
		}

		aux_divider *= 2;
	}
	selected_prescaler = 128;
	return PS_128;
}

uint32_t FTM_claculate_error(uint32_t current_value, uint32_t target_value){
	int32_t error = target_value - current_value;
	if(error<0){
		error *= -1;
	}
	return error;
}

eErrorCodes FTM_pwm_channel_init(FTM_Type* FTM_base,uint8_t channel, FTM_mode_config_t channel_configuration){
	uint32_t freq_aux;
	freq_aux = FTM_frequency/(channel_configuration.PWM_config.PWM_frequency*selected_prescaler);
	FTM_base->MOD = freq_aux - 1;     /* FTM1 counter final value (used for PWM mode) */
	FTM_base -> SC |= FTM_SC_PWMEN0_MASK<<channel; /* enables PWM of respective channel */
	FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_MSB_MASK;
	FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_ELSB_MASK;
	FTM_base -> CONTROLS[channel].CnV = (((FTM_base->MOD) + 1)*channel_configuration.PWM_config.PWM_duty_cycle)/100; /* calculate desired duty cycle value */
	return eNoError;
}

static eErrorCodes FTM_input_capture_channel_init(FTM_Type* FTM_base,uint8_t channel, FTM_mode_config_t channel_configuration){
	FTM_base -> CONTROLS[channel].CnSC &= ~(FTM_CnSC_MSA_MASK | FTM_CnSC_MSB_MASK); /* MSA,MSB as 0 for input capture */
	if(channel_configuration.InputCapture_cofig.InputCapture_mode == capture_rise_edge){
		FTM_base -> CONTROLS[channel].CnSC &= ~FTM_CnSC_ELSB_MASK;	 /* ELSEB = 0, ELSA = 1. Input capture rise edge */
		FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_ELSA_MASK;

	}
	else if(channel_configuration.InputCapture_cofig.InputCapture_mode == capture_fall_edge){
		FTM_base -> CONTROLS[channel].CnSC &= ~FTM_CnSC_ELSA_MASK;	 /* ELSEB = 1, ELSA = 0. Input capture falling edge */
		FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_ELSB_MASK;
	}
	else if(channel_configuration.InputCapture_cofig.InputCapture_mode == capture_any_edge){
		FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_ELSB_MASK;	 /* ELSEB = 1, ELSA = 1. Input capture any edge */
		FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_ELSA_MASK;
	}
	else{
		return eInvalidParameter;
	}

	if(channel_configuration.InputCapture_cofig.interrupt_setting == INT_EN){
		FTM_base -> CONTROLS[channel].CnSC |= FTM_CnSC_CHIE_MASK;
		if(FTM_base == FTM0){
			if(channel == 0 || channel == 1){
				/* enable interrupts */
				EnableIRQ(FTM0_Ch0_Ch1_IRQn);
			}
			else if(channel == 2 || channel == 3){
				/* enable interrupts */
				EnableIRQ(FTM0_Ch2_Ch3_IRQn);
			}
			else if(channel == 4 || channel == 5){
				/* enable interrupts */
				EnableIRQ(FTM0_Ch4_Ch5_IRQn);
			}
			else if(channel == 6 || channel == 7){
				/* enable interrupts */
				EnableIRQ(FTM0_Ch6_Ch7_IRQn);
			}
			FTM0_ch_callback[channel] = channel_configuration.InputCapture_cofig.CallbackPointer;
		}

		else if(FTM_base == FTM1){
			if(channel == 0 || channel == 1){
				/* enable interrupts */
				EnableIRQ(FTM1_Ch0_Ch1_IRQn);
			}
			else if(channel == 2 || channel == 3){
				/* enable interrupts */
				EnableIRQ(FTM1_Ch2_Ch3_IRQn);
			}
			else if(channel == 4 || channel == 5){
				/* enable interrupts */
				EnableIRQ(FTM1_Ch4_Ch5_IRQn);
			}
			else if(channel == 6 || channel == 7){
				/* enable interrupts */
				EnableIRQ(FTM1_Ch6_Ch7_IRQn);
			}
			FTM1_ch_callback[channel] = channel_configuration.InputCapture_cofig.CallbackPointer;
		}
	}
	else{
		FTM_base -> CONTROLS[channel].CnSC &= ~FTM_CnSC_CHIE_MASK;
	}

	return eNoError;
}

static eErrorCodes FTM_output_compare_channel_init(uint8_t channel, FTM_mode_config_t channel_configuration){
	return eNoError;
}


void FTM0_handler(uint8_t channel){
	if(FTM0_ch_callback[channel] != NULL){
		FTM0_ch_callback[channel](FTM0-> CONTROLS[channel].CnV);
	}
}

void FTM1_handler(uint8_t channel){
	if(FTM1_ch_callback[channel] != NULL){
		FTM1_ch_callback[channel](FTM1-> CONTROLS[channel].CnV);
	}
}

void FTM2_handler(uint8_t channel){
	if(FTM2_ch_callback[channel] != NULL){
		FTM2_ch_callback[channel](FTM2-> CONTROLS[channel].CnV);
	}
}

void FTM3_handler(uint8_t channel){
	if(FTM3_ch_callback[channel] != NULL){
		FTM3_ch_callback[channel](FTM3-> CONTROLS[channel].CnV);
	}
}



/**************************************************************************//*!
 *
 * @name    FTM0 ch handlers
 *
 * @brief   Helper funciton for DRIVER module
 *
 * @param   none.
 *
 * @return  none.
 *****************************************************************************/
/* FTM0 ch0 and ch1*/
void FTM0_Ch0_Ch1_IRQHandler(void){
	if(((FTM0 -> CONTROLS[0].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(0);
		FTM0 -> CONTROLS[0].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch0 flag */
	}
	else if(((FTM0 -> CONTROLS[1].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(1);
		FTM0 -> CONTROLS[1].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch1 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM0_Ch2_Ch3_IRQHandler(void){
	if(((FTM0 -> CONTROLS[2].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(2);
		FTM0 -> CONTROLS[2].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch2 flag */
	}
	else if(((FTM0 -> CONTROLS[3].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(3);
		FTM0 -> CONTROLS[3].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch3 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM0_Ch4_Ch5_IRQHandler(void){
	if(((FTM0 -> CONTROLS[4].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(4);
		FTM0 -> CONTROLS[4].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch4 flag */
	}
	else if(((FTM0 -> CONTROLS[5].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(5);
		FTM0 -> CONTROLS[5].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch5 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM0_Ch6_Ch7_IRQHandler(void){
	if(((FTM0 -> CONTROLS[6].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(6);
		FTM0 -> CONTROLS[6].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM6 ch0 flag */
	}
	else if(((FTM0 -> CONTROLS[7].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM0_handler(7);
		FTM0 -> CONTROLS[7].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch7 flag */
	}
}


/**************************************************************************//*!
 *
 * @name    FTM1 ch handlers
 *
 * @brief   Helper funciton for DRIVER module
 *
 * @param   none.
 *
 * @return  none.
 *****************************************************************************/

/* FTM0 ch0 and ch1*/
void FTM1_Ch0_Ch1_IRQHandler(void){
	if(((FTM1 -> CONTROLS[0].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(0);
		FTM1 -> CONTROLS[0].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch0 flag */
	}
	else if(((FTM1 -> CONTROLS[1].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(1);
		FTM1 -> CONTROLS[1].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch1 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM1_Ch2_Ch3_IRQHandler(void){
	if(((FTM1 -> CONTROLS[2].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(2);
		FTM1 -> CONTROLS[2].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch2 flag */
	}
	else if(((FTM1 -> CONTROLS[3].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(3);
		FTM1 -> CONTROLS[3].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch3 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM1_Ch4_Ch5_IRQHandler(void){
	if(((FTM1 -> CONTROLS[4].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(4);
		FTM1 -> CONTROLS[4].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch4 flag */
	}
	else if(((FTM1 -> CONTROLS[5].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(5);
		FTM1 -> CONTROLS[5].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch5 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM1_Ch6_Ch7_IRQHandler(void){
	if(((FTM1 -> CONTROLS[6].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(6);
		FTM1 -> CONTROLS[6].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM6 ch0 flag */
	}
	else if(((FTM1 -> CONTROLS[7].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM1_handler(7);
		FTM1 -> CONTROLS[7].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch7 flag */
	}
}

/**************************************************************************//*!
 *
 * @name    FTM2 ch handlers
 *
 * @brief   Helper funciton for DRIVER module
 *
 * @param   none.
 *
 * @return  none.
 *****************************************************************************/

/* FTM2 ch0 and ch1*/
void FTM2_Ch0_Ch1_IRQHandler(void){
	if(((FTM2 -> CONTROLS[0].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(0);
		FTM2 -> CONTROLS[0].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch0 flag */
	}
	else if(((FTM2 -> CONTROLS[1].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(1);
		FTM2 -> CONTROLS[1].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch1 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM2_Ch2_Ch3_IRQHandler(void){
	if(((FTM2 -> CONTROLS[2].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(2);
		FTM2 -> CONTROLS[2].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch2 flag */
	}
	else if(((FTM2 -> CONTROLS[3].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(3);
		FTM2 -> CONTROLS[3].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch3 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM2_Ch4_Ch5_IRQHandler(void){
	if(((FTM2 -> CONTROLS[4].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(4);
		FTM2 -> CONTROLS[4].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch4 flag */
	}
	else if(((FTM2 -> CONTROLS[5].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(5);
		FTM2 -> CONTROLS[5].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch5 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM2_Ch6_Ch7_IRQHandler(void){
	if(((FTM2 -> CONTROLS[6].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(6);
		FTM2 -> CONTROLS[6].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM6 ch0 flag */
	}
	else if(((FTM2 -> CONTROLS[7].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM2_handler(7);
		FTM2 -> CONTROLS[7].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch7 flag */
	}
}


/**************************************************************************//*!
 *
 * @name    FTM3 ch handlers
 *
 * @brief   Helper funciton for DRIVER module
 *
 * @param   none.
 *
 * @return  none.
 *****************************************************************************/

/* FTM2 ch0 and ch1*/
void FTM3_Ch0_Ch1_IRQHandler(void){
	if(((FTM3 -> CONTROLS[0].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(0);
		FTM3 -> CONTROLS[0].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch0 flag */
	}
	else if(((FTM3 -> CONTROLS[1].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(1);
		FTM3 -> CONTROLS[1].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch1 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM3_Ch2_Ch3_IRQHandler(void){
	if(((FTM3 -> CONTROLS[2].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(2);
		FTM3 -> CONTROLS[2].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch2 flag */
	}
	else if(((FTM3 -> CONTROLS[3].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(3);
		FTM3 -> CONTROLS[3].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch3 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM3_Ch4_Ch5_IRQHandler(void){
	if(((FTM3 -> CONTROLS[4].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(4);
		FTM3 -> CONTROLS[4].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch4 flag */
	}
	else if(((FTM3 -> CONTROLS[5].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(5);
		FTM3 -> CONTROLS[5].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch5 flag */
	}
}

/* FTM0 ch0 and ch1*/
void FTM3_Ch6_Ch7_IRQHandler(void){
	if(((FTM3 -> CONTROLS[6].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(6);
		FTM3 -> CONTROLS[6].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM6 ch0 flag */
	}
	else if(((FTM3 -> CONTROLS[7].CnSC) & FTM_CnSC_CHF_MASK)>>FTM_CnSC_CHF_SHIFT){
		FTM3_handler(7);
		FTM3 -> CONTROLS[7].CnSC &= ~FTM_CnSC_CHF_MASK; /* clean FTM0 ch7 flag */
	}
}



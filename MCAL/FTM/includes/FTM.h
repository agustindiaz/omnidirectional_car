/*
 * template.h
 *
 *  Rev:
 *  Author:
 *  Date:
 */

#ifndef FTM_H_
#define FTM_H_

/* Only include these header files */
#include "system.h"
#include "error_codes.h"
#include "FTM_config.h"

/* Drived Types */

#define FTM_base_frequency 1000000

typedef enum{
	PS_1,
	PS_2,
	PS_4,
	PS_8,
	PS_16,
	PS_32,
	PS_64,
	PS_128,
}FTM_prescaler_selection_t;

typedef struct{
	uint32_t FTM_div_selected;
}FTM_status_t;



/**************************************************************************//*!
 *
 * @name    DRIVER_init
 *
 * @brief   Initialize DRIVER module
 *
 * @param   config_ptr: Configuration structure for this driver.
 *
 * @return  ERROR_OK: If operations was executed correctly
            ERROR_BUSY: If module could not send data.
 *****************************************************************************/
eErrorCodes FTM_init(FTM_config_t *config_ptr);

/**************************************************************************//*!
 *
 * @name    DRIVER_init
 *
 * @brief   Initialize DRIVER module
 *
 * @param   config_ptr: Configuration structure for this driver.
 *
 * @return  ERROR_OK: If operations was executed correctly
            ERROR_BUSY: If module could not send data.
 *****************************************************************************/
eErrorCodes PWM_set_duty(FTM_Type* FTM_base, uint8_t channel, uint8_t duty_cycle);

/**************************************************************************//*!
 *
 * @name    DRIVER_init
 *
 * @brief   Initialize DRIVER module
 *
 * @param   config_ptr: Configuration structure for this driver.
 *
 * @return  ERROR_OK: If operations was executed correctly
            ERROR_BUSY: If module could not send data.
 *****************************************************************************/
uint8_t InputCapture_event(FTM_Type* FTM_base, uint8_t channel);

#endif /* TEMPLATE_H_ */

/*
 * template_config.c
 *
 *  Created on: Feb 10, 2017
 *      Author: B50982
 */


#include "scg_config.h"

SCG_config_t configStruct = {
    .coreFreq       = 80000000,                 /* Set core clock to 80 MHz */
    .busFreq        = 40000000,                 /* Set bus clock frequency to 40 MHz */
    .flashFreq      = MAXIMUM_AVAILABLE_FREQ,   /* Set Flash frequency to maximum available for the current mode */
};

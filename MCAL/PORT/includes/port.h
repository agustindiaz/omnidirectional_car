/*
 * port.h
 *
 *  Created on: Mar 15, 2017
 *      Author: B50982
 */

#ifndef PORT_H_
#define PORT_H_

/* Only include these header files */
#include "error_codes.h"
#include "port_config.h"

/**************************************************************************//*!
 *
 * @name    PORT_init
 *
 * @brief   It Initializes every pin in the config_ptr array.
 *
 * @param   config_ptr: Pointer to Configuration structure array for this driver.
 *                      The last port element in the array should be set to eInvalidPort
 *                      value in order to proper finish the array list.
 *
 * @return  eNoError: If initialization was done without problems.
 *          eNullPointer: If configuration pointer is NULL.
 *          eInvalidParameter: If configuration structure contains invalid values such as
 *                              pin number greater than 31, etc.
 *
 *****************************************************************************/
eErrorCodes PORT_init(PORT_config_t *const config_ptr);

/**************************************************************************//*!
 *
 * @name    PORT_setMux
 *
 * @brief   Configures desired port and pin to desired MUX value. It turns
 *          PORT's clock on to avoid HardFault exception. It can also be used
 *          to disable one specific pin by setting MUX option to eMuxDisabled.
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes PORT_setMux(ePort port, uint8_t pin, eMuxValue mux);

/**************************************************************************//*!
 *
 * @name    PORT_setAttrib
 *
 * @brief   Configures desired port and pin to specific attributes such as
 *          interrut/dma configuration, pull up/down, drive strength and passive
 *          filter settings.
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          port_attributes_t *: Pointer to attribute structure where these
 *                              settings will be taken from.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes PORT_setAttrib(ePort port, uint8_t pin, port_attributes_t * const attributePtr);

/**************************************************************************//*!
 *
 * @name    PORT_disableAllpins
 *
 * @brief   Disable all pins and clock for desired port.
 *
 * @param   ePort: Port to be disabled: ePortA, ePortB, ePortC, ePortD or ePortE.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes PORT_disableAllpins(ePort port);

/**************************************************************************//*!
 *
 * @name    PORT_getInterruptFlags
 *
 * @brief   Get interrupt flag for desired port
 *
 * @param   ePort: Port to take the flags from: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          *flags: pointer where flags from port will be saved.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eNullPointer: If flags pointer is NULL.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes PORT_getInterruptFlags(ePort port, uint32_t * const flags);

/**************************************************************************//*!
 *
 * @name    PORT_clearInterruptFlags
 *
 * @brief   Clear interrupt flag for desired port
 *
 * @param   ePort: Port to clear the flags: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          flags: Flags to be cleared
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes PORT_clearInterruptFlags(ePort port, uint32_t flags);





#endif /* PORT_H_ */

/*
 * gpio.h
 *
 *  Created on: Mar 15, 2017
 *      Author: B50982
 */

#ifndef GPIO_H_
#define GPIO_H_

/* Only include these header files */
#include "error_codes.h"
#include "port_config.h"

/**************************************************************************//*!
 *
 * @name    GPIO_pinInit
 *
 * @brief   It Initializes every pin in the config_ptr array.
 *
 * @param   config_ptr: Pointer to Configuration structure array for this driver.
 *                      The last port element in the array should be set to eInvalidPort
 *                      value in order to proper finish the array list.
 *
 * @return  eNoError: If initialization was done without problems.
 *          eNullPointer: If configuration pointer is NULL.
 *          eInvalidParameter: If configuration structure contains invalid values such as
 *                              pin number greater than 31, etc.
 *
 *****************************************************************************/
eErrorCodes GPIO_pinInit(ePort port, uint8_t pin, eDirection dir);

/**************************************************************************//*!
 *
 * @name    GPIO_readPort
 *
 * @brief   Configures desired port and pin to desired MUX value. It turns
 *          PORT's clock on to avoid HardFault exception. It can also be used
 *          to disable one specific pin by setting MUX option to eMuxDisabled.
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_readPort(ePort port, uint32_t *const value);

/**************************************************************************//*!
 *
 * @name    GPIO_writePort
 *
 * @brief   Configures desired port and pin to specific attributes such as
 *          interrut/dma configuration, pull up/down, drive strength and passive
 *          filter settings.
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          port_attributes_t *: Pointer to attribute structure where these
 *                              settings will be taken from.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_writePort(ePort port, uint32_t value);

/**************************************************************************//*!
 *
 * @name    GPIO_readPin
 *
 * @brief   Disable all pins and clock for desired port.
 *
 * @param   ePort: Port to be disabled: ePortA, ePortB, ePortC, ePortD or ePortE.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_readPin(ePort port, uint8_t pin, uint8_t *const value);

/**************************************************************************//*!
 *
 * @name    GPIO_togglePin
 *
 * @brief   Disable all pins and clock for desired port.
 *
 * @param   ePort: Port to be disabled: ePortA, ePortB, ePortC, ePortD or ePortE.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_togglePin(ePort port, uint8_t pin);

/**************************************************************************//*!
 *
 * @name    GPIO_clearPin
 *
 * @brief   Disable all pins and clock for desired port.
 *
 * @param   ePort: Port to be disabled: ePortA, ePortB, ePortC, ePortD or ePortE.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_clearPin(ePort port, uint8_t pin);

/**************************************************************************//*!
 *
 * @name    GPIO_setPin
 *
 * @brief   Disable all pins and clock for desired port.
 *
 * @param   ePort: Port to be disabled: ePortA, ePortB, ePortC, ePortD or ePortE.
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes GPIO_setPin(ePort port, uint8_t pin);



#endif /* GPIO_H_ */

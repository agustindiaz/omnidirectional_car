/*
 * port.c
 *
 *  Created on: Mar 15, 2017
 *      Author: B50982
 */

#include "port.h"
#include "system.h"
#include "gpio.h"

eErrorCodes PORT_init(PORT_config_t *const config_ptr)
{
    eErrorCodes ret = eNoError;
    /* Valid pointer checking */
    if (NULL != config_ptr)
    {
        /* Pointer to each element in the PORT structure array */
        PORT_config_t *port_ptr = config_ptr;
        /* Repeat the process if valid configuration structure is used or it while the
         * end of the table is still not reached */
        while ((port_ptr->port != eInvalidPort) && (eNoError == ret))
        {
            /* Configure MUX option */
            ret = PORT_setMux(port_ptr->port, port_ptr->pin, port_ptr->mux);
            if (eNoError == ret)
            {
                /* Configure Attributes */
                ret = PORT_setAttrib(port_ptr->port, port_ptr->pin, &port_ptr->attrib);
                /* If configure as GPIO, then, initialize direction for the pin */
                if ((eMuxAsGPIO == port_ptr->mux) && (eNoError == ret))
                {
                    ret = GPIO_pinInit(port_ptr->port, port_ptr->pin, port_ptr->dir);
                }
            }
            /* Point to next configuration structure*/
            port_ptr++;
        }
    }
    else
    {
        ret = eNullPointer;
    }
    return ret;
}

eErrorCodes PORT_setMux(ePort port, uint8_t pin, eMuxValue mux)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            ((mux >= eMuxDisabled) && (mux <= eMux7)) &&
            (pin <= 31))
    {
        PORT_Type *port_ptrs[] = PORT_BASE_PTRS;
        /* Enable clock for port */
        PCC->PCCn[PCC_PORTA_INDEX + (uint8_t)(port - ePortA)] |= PCC_PCCn_CGC_MASK;
        /* Clear MUX field */
        port_ptrs[port - ePortA]->PCR[pin] &= ~PORT_PCR_MUX_MASK;
        /* Write desire MUX value */
        port_ptrs[port - ePortA]->PCR[pin] |= PORT_PCR_MUX(mux);
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes PORT_setAttrib(ePort port, uint8_t pin, port_attributes_t * const attributePtr)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            (pin <= 31) && (NULL != attributePtr))
    {
        PORT_Type *port_ptrs[] = PORT_BASE_PTRS;
        /* Enable clock for port */
        PCC->PCCn[PCC_PORTA_INDEX + (uint8_t)(port - ePortA)] |= PCC_PCCn_CGC_MASK;
        /* Clear IRQC, DSE, Pull Up options */
        port_ptrs[port - ePortA]->PCR[pin] &= ~(PORT_PCR_DSE_MASK |     /* Clear Drive Strength field */
                PORT_PCR_IRQC_MASK |    /* Clear Int/DMA field */
                PORT_PCR_PFE_MASK |     /* Clear Passive filter field */
                PORT_PCR_PE_MASK |      /* Clear Pull Enable field */
                PORT_PCR_PS_MASK);      /* Clear Pull select field */
        /* Configure port settings */
        port_ptrs[port - ePortA]->PCR[pin] |=   PORT_PCR_ISF_MASK |                     /* Clear ISF flag */
                PORT_PCR_IRQC(attributePtr->int_dma) |  /* Set proper IRQC value */
                PORT_PCR_DSE(attributePtr->driveStr) |  /* Set Drive Strength value */
                PORT_PCR_PFE(attributePtr->filter) |    /* Set Passive filter value */
                attributePtr->pull;                     /* Set pull enable and pull select values */
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes PORT_disableAllpins(ePort port)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if ((port >= ePortA) && (port <= ePortE))
    {
        PORT_Type *port_ptrs[] = PORT_BASE_PTRS;
        /* Enable clock for port */
        PCC->PCCn[PCC_PORTA_INDEX + (uint8_t)(port - ePortA)] |= PCC_PCCn_CGC_MASK;
        /* Write to the 0-15 registers, 0 in the first 15:0 bits (It clears MUX field )*/
        port_ptrs[port - ePortA]->GPCLR = PORT_GPCLR_GPWE(0xFFFF) |
                PORT_GPCLR_GPWD(0);
        /* Write to the 31-16 register, 0 in the first 15:0 bits (It clears MUX field )*/
        port_ptrs[port - ePortA]->GPCHR = PORT_GPCHR_GPWE(0xFFFF) |
                PORT_GPCHR_GPWD(0);
        /* As all pins has been disabled, disable clock for this port */
        PCC->PCCn[PCC_PORTA_INDEX + (uint8_t)(port - ePortA)] &= ~PCC_PCCn_CGC_MASK;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}


eErrorCodes PORT_getInterruptFlags(ePort port, uint32_t * const flags)
{
    eErrorCodes ret = eNoError;
    /* Validate pointer */
    if (NULL != flags)
    {
        /* Validate PORT and pin ranges */
        if ((port >= ePortA) && (port <= ePortE))
        {
            PORT_Type *port_ptrs[] = PORT_BASE_PTRS;
            /* Get flags from port */
            *flags = port_ptrs[port - ePortA]->ISFR;
        }
        else
        {
            ret = eInvalidParameter;
        }
    }
    else
    {
        ret = eNullPointer;
    }
    return ret;
}


eErrorCodes PORT_clearInterruptFlags(ePort port, uint32_t flags)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if ((port >= ePortA) && (port <= ePortE))
    {
        PORT_Type *port_ptrs[] = PORT_BASE_PTRS;
        /* Get flags from port */
        port_ptrs[port - ePortA]->ISFR |= flags;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}


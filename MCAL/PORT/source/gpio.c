/*
 * gpio.c
 *
 *  Created on: Mar 15, 2017
 *      Author: B50982
 */

/*
 * gpio.h
 *
 *  Created on: Mar 15, 2017
 *      Author: B50982
 */

/* Only include these header files */
#include "port.h"
#include "system.h"
#include "gpio.h"


eErrorCodes GPIO_pinInit(ePort port, uint8_t pin, eDirection dir)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            (pin <= 31))
    {
        /* Get proper GPIO pointer */
        GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
        if (eInput == dir)
        {
            gpio_ptrs[port - ePortA]->PDDR &= ~(1 << pin);
        }
        else if (eOutput == dir)
        {
            gpio_ptrs[port - ePortA]->PDDR |= (1 << pin);
        }
        else
        {
            ret = eInvalidParameter;
        }
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes GPIO_readPort(ePort port, uint32_t *const value)
{
    eErrorCodes ret = eNoError;
    if (NULL != value)
    {
        /* Validate PORT and pin ranges */
        if ((port >= ePortA) && (port <= ePortE))
        {
            /* Get proper GPIO pointer */
            GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
            /* Read data from port */
            *value = gpio_ptrs[port - ePortA]->PDIR;
        }
        else
        {
            ret = eInvalidParameter;
        }
    }
    else
    {
        ret = eNullPointer;
    }
    return ret;
}

eErrorCodes GPIO_writePort(ePort port, uint32_t value)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if ((port >= ePortA) && (port <= ePortE))
    {
        /* Get proper GPIO pointer */
        GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
        /* Read data from port */
        gpio_ptrs[port - ePortA]->PDOR = value;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes GPIO_readPin(ePort port, uint8_t pin, uint8_t *const value)
{
    eErrorCodes ret = eNoError;
    if (NULL != value)
    {
        /* Validate PORT and pin ranges */
        if (((port >= ePortA) && (port <= ePortE))  &&
                (pin <= 31))
        {
            /* Get proper GPIO pointer */
            GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
            /* Read data from port */
            *value = (gpio_ptrs[port - ePortA]->PDIR >> pin) & 0x1;
        }
        else
        {
            ret = eInvalidParameter;
        }
    }
    else
    {
        ret = eNullPointer;
    }
    return ret;
}

eErrorCodes GPIO_togglePin(ePort port, uint8_t pin)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            (pin <= 31))
    {
        /* Get proper GPIO pointer */
        GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
        /* Read data from port */
        gpio_ptrs[port - ePortA]->PTOR |= 1 << pin;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes GPIO_clearPin(ePort port, uint8_t pin)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            (pin <= 31))
    {
        /* Get proper GPIO pointer */
        GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
        /* Read data from port */
        gpio_ptrs[port - ePortA]->PCOR |= 1 << pin;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}

eErrorCodes GPIO_setPin(ePort port, uint8_t pin)
{
    eErrorCodes ret = eNoError;
    /* Validate PORT and pin ranges */
    if (((port >= ePortA) && (port <= ePortE)) &&
            (pin <= 31))
    {
        /* Get proper GPIO pointer */
        GPIO_Type *gpio_ptrs[] = GPIO_BASE_PTRS;
        /* Read data from port */
        gpio_ptrs[port - ePortA]->PSOR |= 1 << pin;
    }
    else
    {
        ret = eInvalidParameter;
    }
    return ret;
}


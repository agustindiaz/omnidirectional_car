/*
 * port_config.c
 *
 *  Created on: Mar 13, 2017
 *      Author: B50982
 */

#include "OmniRobot_config.h"



PORT_config_t portConfigStruct[] = {

/* GPIOS */

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR1_EN_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR2_EN_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR3_EN_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR4_EN_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR1_INA_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR1_INB_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR2_INA_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR2_INB_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR3_INA_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR3_INB_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR4_INA_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

        {
                .port       = MOTOR_LOGIC_PORT,
                .pin        = MOTOR4_INB_PIN,
                .mux        = eMuxAsGPIO,
                .dir        = eOutput
        },

/* FTM channels (PWMs and input captures) */

        {
                .port       = ePortD,
                .pin        = 15,
                .mux        = eMux2,
        },

        {
                .port       = ePortD,
                .pin        = 16,
                .mux        = eMux2,
        },

        {
                .port       = ePortD,
                .pin        = 0,
                .mux        = eMux2,
        },

        {
                .port       = ePortD,
                .pin        = 1,
                .mux        = eMux2,
        },

        {
                .port       = ePortB,
                .pin        = 2,
                .mux        = eMux2,
        },

        {
                .port       = ePortB,
                .pin        = 3,
                .mux        = eMux2,
        },

        {
                .port       = ePortA,
                .pin        = 15,
                .mux        = eMux2,
        },

        {
                .port       = ePortC,
                .pin        = 15,
                .mux        = eMux2,
        },


/* ADC channels */
        {
                .port       = ePortA,
                .pin        = 0,
                .mux        = eMuxDisabled,
        },

        {
                .port       = ePortA,
                .pin        = 1,
                .mux        = eMuxDisabled,
        },

        {
                .port       = ePortA,
                .pin        = 6,
                .mux        = eMuxDisabled,
        },

        {
                .port       = ePortA,
                .pin        = 7,
                .mux        = eMuxDisabled,
        },

/* UART pins */
        {
                .port       = ePortC,
                .pin        = 6,
                .mux        = eMux2,
        },

        {
                .port       = ePortC,
                .pin        = 7,
                .mux        = eMux2,
        },



        {
                .port       = eInvalidPort
        }
};


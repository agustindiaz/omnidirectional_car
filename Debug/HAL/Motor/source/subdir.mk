################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../HAL/Motor/source/Motor.c" \

C_SRCS += \
../HAL/Motor/source/Motor.c \

OBJS_OS_FORMAT += \
./HAL/Motor/source/Motor.o \

C_DEPS_QUOTED += \
"./HAL/Motor/source/Motor.d" \

OBJS += \
./HAL/Motor/source/Motor.o \

OBJS_QUOTED += \
"./HAL/Motor/source/Motor.o" \

C_DEPS += \
./HAL/Motor/source/Motor.d \


# Each subdirectory must supply rules for building sources it contributes
HAL/Motor/source/Motor.o: ../HAL/Motor/source/Motor.c
	@echo 'Building file: $<'
	@echo 'Executing target #15 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@HAL/Motor/source/Motor.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "HAL/Motor/source/Motor.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



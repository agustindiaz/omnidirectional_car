################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../HAL/OmniRobot/source/OmniRobot.c" \

C_SRCS += \
../HAL/OmniRobot/source/OmniRobot.c \

OBJS_OS_FORMAT += \
./HAL/OmniRobot/source/OmniRobot.o \

C_DEPS_QUOTED += \
"./HAL/OmniRobot/source/OmniRobot.d" \

OBJS += \
./HAL/OmniRobot/source/OmniRobot.o \

OBJS_QUOTED += \
"./HAL/OmniRobot/source/OmniRobot.o" \

C_DEPS += \
./HAL/OmniRobot/source/OmniRobot.d \


# Each subdirectory must supply rules for building sources it contributes
HAL/OmniRobot/source/OmniRobot.o: ../HAL/OmniRobot/source/OmniRobot.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@HAL/OmniRobot/source/OmniRobot.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "HAL/OmniRobot/source/OmniRobot.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



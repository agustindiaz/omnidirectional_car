################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../HAL/Encoder/source/Encoder.c" \

C_SRCS += \
../HAL/Encoder/source/Encoder.c \

OBJS_OS_FORMAT += \
./HAL/Encoder/source/Encoder.o \

C_DEPS_QUOTED += \
"./HAL/Encoder/source/Encoder.d" \

OBJS += \
./HAL/Encoder/source/Encoder.o \

OBJS_QUOTED += \
"./HAL/Encoder/source/Encoder.o" \

C_DEPS += \
./HAL/Encoder/source/Encoder.d \


# Each subdirectory must supply rules for building sources it contributes
HAL/Encoder/source/Encoder.o: ../HAL/Encoder/source/Encoder.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@HAL/Encoder/source/Encoder.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "HAL/Encoder/source/Encoder.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



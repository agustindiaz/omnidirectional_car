################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/ECG/config/scg_config.c" \

C_SRCS += \
../MCAL/ECG/config/scg_config.c \

OBJS_OS_FORMAT += \
./MCAL/ECG/config/scg_config.o \

C_DEPS_QUOTED += \
"./MCAL/ECG/config/scg_config.d" \

OBJS += \
./MCAL/ECG/config/scg_config.o \

OBJS_QUOTED += \
"./MCAL/ECG/config/scg_config.o" \

C_DEPS += \
./MCAL/ECG/config/scg_config.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/ECG/config/scg_config.o: ../MCAL/ECG/config/scg_config.c
	@echo 'Building file: $<'
	@echo 'Executing target #13 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/ECG/config/scg_config.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/ECG/config/scg_config.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



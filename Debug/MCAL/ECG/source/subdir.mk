################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/ECG/source/scg.c" \

C_SRCS += \
../MCAL/ECG/source/scg.c \

OBJS_OS_FORMAT += \
./MCAL/ECG/source/scg.o \

C_DEPS_QUOTED += \
"./MCAL/ECG/source/scg.d" \

OBJS += \
./MCAL/ECG/source/scg.o \

OBJS_QUOTED += \
"./MCAL/ECG/source/scg.o" \

C_DEPS += \
./MCAL/ECG/source/scg.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/ECG/source/scg.o: ../MCAL/ECG/source/scg.c
	@echo 'Building file: $<'
	@echo 'Executing target #12 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/ECG/source/scg.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/ECG/source/scg.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/FTM/source/FTM.c" \

C_SRCS += \
../MCAL/FTM/source/FTM.c \

OBJS_OS_FORMAT += \
./MCAL/FTM/source/FTM.o \

C_DEPS_QUOTED += \
"./MCAL/FTM/source/FTM.d" \

OBJS += \
./MCAL/FTM/source/FTM.o \

OBJS_QUOTED += \
"./MCAL/FTM/source/FTM.o" \

C_DEPS += \
./MCAL/FTM/source/FTM.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/FTM/source/FTM.o: ../MCAL/FTM/source/FTM.c
	@echo 'Building file: $<'
	@echo 'Executing target #10 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/FTM/source/FTM.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/FTM/source/FTM.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/FTM/config/FTM_config.c" \

C_SRCS += \
../MCAL/FTM/config/FTM_config.c \

OBJS_OS_FORMAT += \
./MCAL/FTM/config/FTM_config.o \

C_DEPS_QUOTED += \
"./MCAL/FTM/config/FTM_config.d" \

OBJS += \
./MCAL/FTM/config/FTM_config.o \

OBJS_QUOTED += \
"./MCAL/FTM/config/FTM_config.o" \

C_DEPS += \
./MCAL/FTM/config/FTM_config.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/FTM/config/FTM_config.o: ../MCAL/FTM/config/FTM_config.c
	@echo 'Building file: $<'
	@echo 'Executing target #11 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/FTM/config/FTM_config.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/FTM/config/FTM_config.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



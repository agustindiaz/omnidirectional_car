################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/LPUART/source/lpuart.c" \

C_SRCS += \
../MCAL/LPUART/source/lpuart.c \

OBJS_OS_FORMAT += \
./MCAL/LPUART/source/lpuart.o \

C_DEPS_QUOTED += \
"./MCAL/LPUART/source/lpuart.d" \

OBJS += \
./MCAL/LPUART/source/lpuart.o \

OBJS_QUOTED += \
"./MCAL/LPUART/source/lpuart.o" \

C_DEPS += \
./MCAL/LPUART/source/lpuart.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/LPUART/source/lpuart.o: ../MCAL/LPUART/source/lpuart.c
	@echo 'Building file: $<'
	@echo 'Executing target #9 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/LPUART/source/lpuart.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/LPUART/source/lpuart.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



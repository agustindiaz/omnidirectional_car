################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/PORT/source/gpio.c" \
"../MCAL/PORT/source/port.c" \

C_SRCS += \
../MCAL/PORT/source/gpio.c \
../MCAL/PORT/source/port.c \

OBJS_OS_FORMAT += \
./MCAL/PORT/source/gpio.o \
./MCAL/PORT/source/port.o \

C_DEPS_QUOTED += \
"./MCAL/PORT/source/gpio.d" \
"./MCAL/PORT/source/port.d" \

OBJS += \
./MCAL/PORT/source/gpio.o \
./MCAL/PORT/source/port.o \

OBJS_QUOTED += \
"./MCAL/PORT/source/gpio.o" \
"./MCAL/PORT/source/port.o" \

C_DEPS += \
./MCAL/PORT/source/gpio.d \
./MCAL/PORT/source/port.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/PORT/source/gpio.o: ../MCAL/PORT/source/gpio.c
	@echo 'Building file: $<'
	@echo 'Executing target #6 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/PORT/source/gpio.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/PORT/source/gpio.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

MCAL/PORT/source/port.o: ../MCAL/PORT/source/port.c
	@echo 'Building file: $<'
	@echo 'Executing target #7 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/PORT/source/port.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/PORT/source/port.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../MCAL/PORT/config/port_config.c" \

C_SRCS += \
../MCAL/PORT/config/port_config.c \

OBJS_OS_FORMAT += \
./MCAL/PORT/config/port_config.o \

C_DEPS_QUOTED += \
"./MCAL/PORT/config/port_config.d" \

OBJS += \
./MCAL/PORT/config/port_config.o \

OBJS_QUOTED += \
"./MCAL/PORT/config/port_config.o" \

C_DEPS += \
./MCAL/PORT/config/port_config.d \


# Each subdirectory must supply rules for building sources it contributes
MCAL/PORT/config/port_config.o: ../MCAL/PORT/config/port_config.c
	@echo 'Building file: $<'
	@echo 'Executing target #8 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@MCAL/PORT/config/port_config.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "MCAL/PORT/config/port_config.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../ECUAL/Encoder/source/Encoder.c" \

C_SRCS += \
../ECUAL/Encoder/source/Encoder.c \

OBJS_OS_FORMAT += \
./ECUAL/Encoder/source/Encoder.o \

C_DEPS_QUOTED += \
"./ECUAL/Encoder/source/Encoder.d" \

OBJS += \
./ECUAL/Encoder/source/Encoder.o \

OBJS_QUOTED += \
"./ECUAL/Encoder/source/Encoder.o" \

C_DEPS += \
./ECUAL/Encoder/source/Encoder.d \


# Each subdirectory must supply rules for building sources it contributes
ECUAL/Encoder/source/Encoder.o: ../ECUAL/Encoder/source/Encoder.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@ECUAL/Encoder/source/Encoder.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "ECUAL/Encoder/source/Encoder.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



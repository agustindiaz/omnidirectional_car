################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../ECUAL/OmniRobot/source/OmniRobot.c" \

C_SRCS += \
../ECUAL/OmniRobot/source/OmniRobot.c \

OBJS_OS_FORMAT += \
./ECUAL/OmniRobot/source/OmniRobot.o \

C_DEPS_QUOTED += \
"./ECUAL/OmniRobot/source/OmniRobot.d" \

OBJS += \
./ECUAL/OmniRobot/source/OmniRobot.o \

OBJS_QUOTED += \
"./ECUAL/OmniRobot/source/OmniRobot.o" \

C_DEPS += \
./ECUAL/OmniRobot/source/OmniRobot.d \


# Each subdirectory must supply rules for building sources it contributes
ECUAL/OmniRobot/source/OmniRobot.o: ../ECUAL/OmniRobot/source/OmniRobot.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@ECUAL/OmniRobot/source/OmniRobot.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "ECUAL/OmniRobot/source/OmniRobot.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../SERVICES/nvic/nvic.c" \

C_SRCS += \
../SERVICES/nvic/nvic.c \

OBJS_OS_FORMAT += \
./SERVICES/nvic/nvic.o \

C_DEPS_QUOTED += \
"./SERVICES/nvic/nvic.d" \

OBJS += \
./SERVICES/nvic/nvic.o \

OBJS_QUOTED += \
"./SERVICES/nvic/nvic.o" \

C_DEPS += \
./SERVICES/nvic/nvic.d \


# Each subdirectory must supply rules for building sources it contributes
SERVICES/nvic/nvic.o: ../SERVICES/nvic/nvic.c
	@echo 'Building file: $<'
	@echo 'Executing target #2 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@SERVICES/nvic/nvic.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "SERVICES/nvic/nvic.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '



/*
 * encoder_config.h
 *
 *  Created on: Jun 26, 2017
 *      Author: B55840
 */

#ifndef ENCODER_CONFIG_ENCODER_CONFIG_H_
#define ENCODER_CONFIG_ENCODER_CONFIG_H_

#define OMNIROBOT_ENCODERS 	FTM1



/* Encoder pins */
#define MOTOR1_ENCODER_CHANNEL		0
#define MOTOR2_ENCODER_CHANNEL		1
#define MOTOR3_ENCODER_CHANNEL		2
#define MOTOR4_ENCODER_CHANNEL		3

#endif /* ENCODER_CONFIG_ENCODER_CONFIG_H_ */

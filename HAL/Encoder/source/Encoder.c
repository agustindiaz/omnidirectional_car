
#include "Encoder.h"
#include "OmniRobot_config.h"
#include "error_codes.h"
#include "system.h"
#include "FTM.h"
#include "gpio.h"

/* configuration structure */
extern FTM_config_t FTM_Encoder_config;

/* callback functions for pulse detection */
FTM_callback encoder1_value(uint32_t compare_value);
FTM_callback encoder2_value(uint32_t compare_value);
FTM_callback encoder3_value(uint32_t compare_value);
FTM_callback encoder4_value(uint32_t compare_value);

/* global variables */
static uint32_t pulse_counter[4] = {0};
static uint16_t encoder1_time_value = 0;
static uint16_t encoder2_time_value = 0;
static uint16_t encoder3_time_value = 0;
static uint16_t encoder4_time_value = 0;


eErrorCodes Encoder_init(){
	FTM_init(&FTM_Encoder_config);
	return eNoError;
}

eErrorCodes Encoder_get_val(uint16_t* array_values){
	array_values[0] = encoder1_time_value;
	array_values[1] = encoder2_time_value;
	array_values[2] = encoder3_time_value;
	array_values[3] = encoder4_time_value;
}


FTM_callback encoder1_value(uint32_t compare_value){
	static uint16_t old_encoder_value;
	pulse_counter[0] += pulse_counter[0];
	if(compare_value<old_encoder_value){
		encoder1_time_value = compare_value - old_encoder_value;
	}
	else{
		old_encoder_value = 0xFFFF - old_encoder_value;
		encoder1_time_value = old_encoder_value + compare_value;

	}
	old_encoder_value = compare_value;
}

FTM_callback encoder2_value(uint32_t compare_value){
	static uint16_t old_encoder_value;
	pulse_counter[1] += pulse_counter[1];
	if(compare_value<old_encoder_value){
		encoder2_time_value = compare_value - old_encoder_value;
	}
	else{
		old_encoder_value = 0xFFFF - old_encoder_value;
		encoder2_time_value = old_encoder_value + compare_value;
	}
	old_encoder_value = compare_value;
}

FTM_callback encoder3_value(uint32_t compare_value){
	static uint16_t old_encoder_value;
	pulse_counter[2] += pulse_counter[2];
	if(compare_value<old_encoder_value){
		encoder3_time_value = compare_value - old_encoder_value;
	}
	else{
		old_encoder_value = 0xFFFF - old_encoder_value;
		encoder3_time_value = old_encoder_value + compare_value;

	}
	old_encoder_value = compare_value;
}

FTM_callback encoder4_value(uint32_t compare_value){
	static uint16_t old_encoder_value;
	pulse_counter[3] += pulse_counter[3];
	if(compare_value<old_encoder_value){
		encoder4_time_value = compare_value - old_encoder_value;
	}
	else{
		old_encoder_value = 0xFFFF - old_encoder_value;
		encoder4_time_value = old_encoder_value + compare_value;

	}
	old_encoder_value = compare_value;
}

/* includes */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "system.h"
#include "error_codes.h"

/**************************************************************************//*!
 *
 * @name    Motor_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Encoder_init();

/**************************************************************************//*!
 *
 * @name    OmniRobot_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Encoder_get_val(uint16_t* array_values);




#endif

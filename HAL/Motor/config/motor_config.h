/*
 * motor_config.h
 *
 *  Created on: Jun 26, 2017
 *      Author: B55840
 */

#ifndef MOTOR_CONFIG_MOTOR_CONFIG_H_
#define MOTOR_CONFIG_MOTOR_CONFIG_H_

#include "port_config.h"


#define OMNIROBOT_MOTORS FTM0

#define MOTOR_LOGIC_PORT	ePortE

/* Motor 1 logic outputs */
#define MOTOR1_INA_PIN		4
#define MOTOR1_INB_PIN		5
#define MOTOR1_EN_PIN		7

/* Motor 2 logic outputs */
#define MOTOR2_INA_PIN		2
#define MOTOR2_INB_PIN		3
#define MOTOR2_EN_PIN		8

/* Motor 3 logic outputs */
#define MOTOR3_INA_PIN		13
#define MOTOR3_INB_PIN		14
#define MOTOR3_EN_PIN		9

/* Motor 4 logic outputs */
#define MOTOR4_INA_PIN		15
#define MOTOR4_INB_PIN		16
#define MOTOR4_EN_PIN		12

/* Motor 1 channel */
#define MOTOR1_PWM_CHANNEL			0

/* Motor 2 FTM channel */
#define MOTOR2_PWM_CHANNEL			1

/* Motor 3 FTM channel */
#define MOTOR3_PWM_CHANNEL			2

/* Motor 4 FTM channel */
#define MOTOR4_PWM_CHANNEL			3

#endif /* MOTOR_CONFIG_MOTOR_CONFIG_H_ */

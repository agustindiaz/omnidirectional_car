/* includes */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "system.h"
#include "error_codes.h"

typedef enum{
	Front,
	Back
}Motor_direction;

/**************************************************************************//*!
 *
 * @name    Motor_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Motor_init();

/**************************************************************************//*!
 *
 * @name    OmniRobot_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Motor_move(Motor_direction direction, uint8_t Motor_channel, uint8_t Motor_duty);




#endif


#include "Motor.h"
#include "motor_config.h"
#include "error_codes.h"
#include "system.h"
#include "FTM.h"
#include "gpio.h"


eErrorCodes Motor_move(Motor_direction direction, uint8_t Motor_channel, uint8_t Motor_duty){
	if(direction == Front){
		if(Motor_channel == MOTOR1_PWM_CHANNEL){
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR1_INA_PIN);
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR1_INB_PIN);
		}
		else if(Motor_channel == MOTOR2_PWM_CHANNEL){
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR2_INA_PIN);
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR2_INB_PIN);
		}

		else if(Motor_channel == MOTOR3_PWM_CHANNEL){
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR3_INA_PIN);
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR3_INB_PIN);

		}

		else if(Motor_channel == MOTOR4_PWM_CHANNEL){
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR4_INA_PIN);
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR4_INB_PIN);

		}
	}
	else if(direction == Back){
		if(Motor_channel == MOTOR1_PWM_CHANNEL){
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR1_INA_PIN);
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR1_INB_PIN);
		}
		else if(Motor_channel == MOTOR2_PWM_CHANNEL){
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR2_INA_PIN);
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR2_INB_PIN);
		}

		else if(Motor_channel == MOTOR3_PWM_CHANNEL){
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR3_INA_PIN);
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR3_INB_PIN);
		}

		else if(Motor_channel == MOTOR4_PWM_CHANNEL){
			GPIO_clearPin(MOTOR_LOGIC_PORT,MOTOR4_INA_PIN);
			GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR4_INB_PIN);
		}
	}
	else{
		return eInvalidParameter;
	}

	PWM_set_duty(OMNIROBOT_MOTORS, Motor_channel, Motor_duty);

	return eNoError;
}

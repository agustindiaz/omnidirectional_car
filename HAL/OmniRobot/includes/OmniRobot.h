/* includes */

#ifndef OMNIROBOT_H_
#define OMNIROBOT_H_

#include "system.h"
#include "error_codes.h"

typedef enum{
	Foward,
	Backward,
	Right,
	Left,
	Diagonal_up_right,
	Diagonal_up_left,
	Diagonal_down_right,
	Diagonal_down_left
}OmniRobot_direction;

/**************************************************************************//*!
 *
 * @name    OmniRobot_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Omnirobot_init();

/**************************************************************************//*!
 *
 * @name    OmniRobot_init()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Omnirobot_move(OmniRobot_direction direction, uint8_t Motor1_duty, uint8_t Motor2_duty,
							uint8_t Motor3_duty, uint8_t Motor4_duty);

/**************************************************************************//*!
 *
 * @name    OmniRobot_stop()
 *
 * @brief   Initializes all functions needed by the robot
 *
 * @param   ePort: Port to configure: ePortA, ePortB, ePortC, ePortD or ePortE.
 *          uint8_t pin: Pin number. Valid values are 0 to 31.
 *          eMuxValue: MUX option: eMuxDisabled, eMuxAsGPIO, eMux2, ... , eMux7
 *
 * @return  eNoError: Configuration was done without problems.
 *          eInvalidParameter: function received invalid parameters.
 *****************************************************************************/
eErrorCodes Omnirobot_stop(void);



#endif

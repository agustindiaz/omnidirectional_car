
#include "OmniRobot.h"
#include "Motor.h"
//#include "Encoder.h"
#include "FTM.h"
#include "nvic.h"
#include "port.h"
#include "OmniRobot_config.h"



/* configuration structures */
extern FTM_config_t FTM_Motor_config;
extern PORT_config_t portConfigStruct[];

//static FTM_callback FTM0_ch3_action(uint32_t capture_value){
//	uint32_t dummy = capture_value;
//}


static void OmniRobot_set_motors_speed(uint8_t M1_speed, uint8_t M2_speed, uint8_t M3_speed, uint8_t M4_speed);

eErrorCodes Omnirobot_init(){
	EnableIRQ(FTM0_Ch2_Ch3_IRQn);
    PORT_init(&portConfigStruct[0]);
	FTM_init(&FTM_Motor_config);
	GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR1_EN_PIN);
	GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR2_EN_PIN);
	GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR3_EN_PIN);
	GPIO_setPin(MOTOR_LOGIC_PORT,MOTOR4_EN_PIN);
	//Encoder_init();
}

eErrorCodes Omnirobot_move(OmniRobot_direction direction,
							uint8_t Motor1_duty,
							uint8_t Motor2_duty,
							uint8_t Motor3_duty,
							uint8_t Motor4_duty){

	switch(direction){
		case Foward:
			Motor_move(Front,MOTOR1_PWM_CHANNEL,Motor1_duty);
			Motor_move(Front,MOTOR2_PWM_CHANNEL,Motor2_duty);
			Motor_move(Front,MOTOR3_PWM_CHANNEL,Motor3_duty);
			Motor_move(Front,MOTOR4_PWM_CHANNEL,Motor4_duty);
			break;
		case Backward:
			Motor_move(Back,MOTOR1_PWM_CHANNEL,Motor1_duty);
			Motor_move(Back,MOTOR2_PWM_CHANNEL,Motor2_duty);
			Motor_move(Back,MOTOR3_PWM_CHANNEL,Motor3_duty);
			Motor_move(Back,MOTOR4_PWM_CHANNEL,Motor4_duty);
			break;
		case Right:
//			Motor_move(Front,MOTOR1_PWM_CHANNEL,Motor1_duty);
//			Motor_move(Back,MOTOR2_PWM_CHANNEL,Motor2_duty);
//			Motor_move(Back,MOTOR3_PWM_CHANNEL,Motor3_duty);
//			Motor_move(Front,MOTOR4_PWM_CHANNEL,Motor4_duty);
			Motor_move(Front,MOTOR1_PWM_CHANNEL,Motor1_duty);
			Motor_move(Front,MOTOR2_PWM_CHANNEL,Motor2_duty);
			Motor_move(Back,MOTOR3_PWM_CHANNEL,0);
			Motor_move(Back,MOTOR4_PWM_CHANNEL,0);

			break;
		case Left:
//			Motor_move(Back,MOTOR1_PWM_CHANNEL,Motor1_duty);
//			Motor_move(Front,MOTOR2_PWM_CHANNEL,Motor2_duty);
//			Motor_move(Front,MOTOR3_PWM_CHANNEL,Motor3_duty);
//			Motor_move(Back,MOTOR4_PWM_CHANNEL,Motor4_duty);
			Motor_move(Back,MOTOR1_PWM_CHANNEL,0);
			Motor_move(Back,MOTOR2_PWM_CHANNEL,0);
			Motor_move(Front,MOTOR3_PWM_CHANNEL,Motor3_duty);
			Motor_move(Front,MOTOR4_PWM_CHANNEL,Motor4_duty);
			break;
		case Diagonal_up_right:
			Motor_move(Front,MOTOR1_PWM_CHANNEL,Motor1_duty);
			Motor_move(Front,MOTOR2_PWM_CHANNEL,0);
			Motor_move(Front,MOTOR3_PWM_CHANNEL,Motor3_duty);
			Motor_move(Front,MOTOR4_PWM_CHANNEL,0);
			break;
		case Diagonal_up_left:
			Motor_move(Back,MOTOR1_PWM_CHANNEL,Motor1_duty);
			Motor_move(Front,MOTOR2_PWM_CHANNEL,0);
			Motor_move(Back,MOTOR3_PWM_CHANNEL,Motor3_duty);
			Motor_move(Front,MOTOR4_PWM_CHANNEL,0);
			break;
		case Diagonal_down_right:
			Motor_move(Front,MOTOR1_PWM_CHANNEL,0);
			Motor_move(Front,MOTOR2_PWM_CHANNEL,Motor2_duty);
			Motor_move(Front,MOTOR3_PWM_CHANNEL,0);
			Motor_move(Front,MOTOR4_PWM_CHANNEL,Motor4_duty);
			break;
		case Diagonal_down_left:
			Motor_move(Front,MOTOR1_PWM_CHANNEL,0);
			Motor_move(Back,MOTOR2_PWM_CHANNEL,Motor2_duty);
			Motor_move(Front,MOTOR3_PWM_CHANNEL,0);
			Motor_move(Back,MOTOR4_PWM_CHANNEL,Motor4_duty);
			break;
		default:
			return eInvalidParameter;
			break;
	}
	return eNoError;
}


void OmniRobot_set_motors_speed(uint8_t M1_speed, uint8_t M2_speed, uint8_t M3_speed, uint8_t M4_speed){
	PWM_set_duty(OMNIROBOT_MOTORS,MOTOR1_PWM_CHANNEL,M1_speed);
	PWM_set_duty(OMNIROBOT_MOTORS,MOTOR2_PWM_CHANNEL,M2_speed);
	PWM_set_duty(OMNIROBOT_MOTORS,MOTOR3_PWM_CHANNEL,M3_speed);
	PWM_set_duty(OMNIROBOT_MOTORS,MOTOR4_PWM_CHANNEL,M4_speed);
}

